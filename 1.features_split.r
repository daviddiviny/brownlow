##Load data -----
load("allyearslist.RDS")
load("all_players.RDS")
library(dplyr)
library(tidyr)
test_year<-2016
model_year<-2017
##Clean data and create new variables----
all_years_df<-bind_rows(all_years) %>%
        select(-c(urls, club, player_id))%>%
        mutate_at(vars(c(K:SC, home.score, away.score)), funs(as.numeric(.))) %>%
        mutate(winningteam = case_when(
                away.score == home.score ~ "draw",
                away.score > home.score ~ "away.team",
                home.score > away.score~ "home.team"
        ),
        winner = case_when(
                winningteam == "draw" ~ TRUE,
                team == winningteam ~ TRUE,
                team != winningteam ~ FALSE
        ),
        winningmargin = ifelse(winner == TRUE, abs(home.score - away.score),0)
        ) %>%
        mutate(vote_binary = if_else(votes != 0, "votes", "novotes", "novotes"))
all_years_df$votes[is.na(all_years_df$votes)]<-0
all_years_df$vote_binary<-factor(all_years_df$vote_binary)

#Join position from player data
all_years_players_df<-left_join(all_years_df, all_players_df, by = c("player_url" = "playerid"))
### Prepare data for modelling ----
data<-all_years_players_df %>%
        group_by(Id) %>%
        mutate_at(vars(c(K:M, T:SC)), funs(Scale = scale)) %>%
        dplyr::select(-c(K:M, T:SC)) %>%
        mutate(winningmargin = ifelse(winner == TRUE, abs(home.score - away.score),0)) %>% 
        #gamewinner = G * winningmargin) %>%
        ungroup()
data<-mutate_at(data, vars("team","votes", "winningteam", "winner", "position"), funs(factor(.))) 
modeldata<-dplyr::select(data, -c(Player, player_url, round,Id, Home, Away,home.score,away.score, player_url))
#Split data into training and test sets
traindata<-filter(modeldata,
                  year < test_year, year != 2012) %>%
        dplyr::select(-c(year, votes))
testdata<-filter(modeldata,
                 year ==test_year) %>%
        dplyr::select(-c(year)) 
data_test_year<-filter(data,
                       year ==test_year) 
finaldata<-filter(data, 
                  year == model_year) %>%
        select(-c(votes, vote_binary))
