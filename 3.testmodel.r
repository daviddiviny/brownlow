#model<-fit.rf
#load data      
test_predict<-testdata
#predict probability of votes - svm
svmprob<-predict(fit.svm, testdata, probability = T)
test_predict$svmprob<-attr(svmprob, "probabilities")[,2]
#predict probability of votes - rf
test_predict$rfprob<-predict(fit.rf, testdata, type ="prob")[,2]
#bring over other data
test_predict$Player<-data_test_year$Player
test_predict$Id<-data_test_year$Id
#For each match, give three votes to highest probability player etc
test_predict<-test_predict %>% group_by(Id) %>%
        mutate(svmrank = rank(desc(svmprob)),
               svmvote = ifelse(svmrank ==1, 3,
                                    ifelse(svmrank == 2, 2,
                                           ifelse(svmrank == 3, 1, 0))),
               rfrank = rank(desc(rfprob)),
               rfvote = ifelse(rfrank ==1, 3,
                                ifelse(rfrank == 2, 2,
                                       ifelse(rfrank == 3, 1, 0)))) %>%
        ungroup()
#Summarise for year
test_predict_summary<-test_predict %>% group_by(Player) %>%
        summarise(Actual = sum(as.numeric(as.character(votes)), na.rm = T),
                  svmvotes = sum(as.numeric(as.character(svmvote)), na.rm = T),
                  rfvotes = sum(as.numeric(as.character(rfvote)), na.rm = T),
                  avgvotes = (svmvotes + rfvotes)/2) %>%
        mutate(Rank_Actual = min_rank(desc(Actual)),
               Rank_svmvotes = min_rank(desc(svmvotes)),
               Rank_rfvotes = min_rank(desc(rfvotes)),
               Rank_avgvotes = min_rank(desc(avgvotes)))%>%
        arrange(Rank_Actual)


#calculate the number of correct predictions for the top n
metricthreshold<-5
metric<-sum(test_predict_summary$Player[test_predict_summary$Rank_Predicted <=metricthreshold] %in% 
                    test_predict_summary$Player[test_predict_summary$Rank_Actual <=metricthreshold])/length(test_predict_summary$Player[test_predict_summary$Rank_Predicted <=metricthreshold])   


finalpred<-predict(fit.svm, finaldata, probability = T)
final_predict<-finaldata

final_predict$svmprob<-attr(finalpred, "probabilities")[,2]# svm
final_predict$rfprob<-predict(fit.rf, final_predict, type ="prob")[,2] #rf

#For each match, give three votes to highest probability player etc
final_predict<-final_predict %>% group_by(Id) %>%
        mutate(svmrank = rank(desc(svmprob)),
               svmvote = ifelse(svmrank ==1, 3,
                                    ifelse(svmrank == 2, 2,
                                           ifelse(svmrank == 3, 1, 0)))) %>%
        ungroup()
#Summarise for year
final_predict_summary<-final_predict %>% group_by(Player) %>%
        summarise(svmvotes = sum(as.numeric(as.character(svmvote)), na.rm = T)) %>%
        mutate(Rank_svmvotes = min_rank(desc(svmvotes)))%>%
        arrange(Rank_svmvotes)
